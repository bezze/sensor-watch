/*
 * MIT License
 *
 * Copyright (c) 2024 electrochori
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>
#include <string.h>
#include "watch_utility.h"
#include "ferrochrono_face.h"

#include "ferrochrono/mitre_rt.c"
#include "ferrochrono/sanmartin_rp.c"

Schedule *schedules[] = {
    &MRT_Tigre_Wd_Schedule,
    &MRT_Retiro_Wd_Schedule,
    &MRT_Tigre_Alt_Schedule,
    &MRT_Retiro_Alt_Schedule,

    &SMRP_Pilar_Wd_Schedule,
    &SMRP_Retiro_Wd_Schedule,
    &SMRP_Pilar_Sat_Schedule,
    &SMRP_Retiro_Sat_Schedule,
    &SMRP_Pilar_Hd_Schedule,
    &SMRP_Retiro_Hd_Schedule,
};

Stations *stations[] = {
    &MRT_Tigre_Wd_Stations,
    &MRT_Retiro_Wd_Stations,
    &MRT_Tigre_Alt_Stations,
    &MRT_Retiro_Alt_Stations,

    &SMRP_Pilar_Wd_Stations,
    &SMRP_Retiro_Wd_Stations,
    &SMRP_Pilar_Sat_Stations,
    &SMRP_Retiro_Sat_Stations,
    &SMRP_Pilar_Hd_Stations,
    &SMRP_Retiro_Hd_Stations,
};

#define NUM_LINES (sizeof(schedules) / sizeof(Schedule))

void ferrochrono_face_setup(movement_settings_t *settings, uint8_t watch_face_index, void ** context_ptr) {
    (void) settings;
    (void) watch_face_index;
    if (*context_ptr == NULL) {
        *context_ptr = malloc(sizeof(ferrochrono_state_t));
        memset(*context_ptr, 0, sizeof(ferrochrono_state_t));
        // Do any one-time tasks in here; the inside of this conditional happens only at boot.
        ferrochrono_state_t *state = (ferrochrono_state_t *)*context_ptr;
        state->mode = rw_select;
        state->rw_line = 0;
        state->rw_station = 0;
        state->show_rel = SHOW_REL_TIME;
    }
    // Do any pin or peripheral setup here; this will be called whenever the watch wakes from deep sleep.
}


static void update_screen_line_sel(uint8_t lineNumber) {
    const Stations *lineStations = stations[lineNumber];
    const Line * currLine = lineStations->line;
    watch_clear_colon();
    watch_display_string("FC ", 0); // we clean 3rd position

    printf("%2d|%s|%d\n", lineNumber+1, currLine->name, currLine->day_mask);

    char buf_ln[3] = "   ";
    sprintf(buf_ln, "%2d", lineNumber+1);
    watch_display_string(buf_ln, 2);

    char buf[7] = "       ";
    sprintf(buf, "%s%d", currLine->name, currLine->day_mask);
    watch_display_string(buf, 4);

}

static uint8_t seek_next_train(uint8_t lineNumber, uint8_t stationNumber, SchedCell* sched) {
    const Schedule *lineSchedule = schedules[lineNumber];
    const Line * currLine = lineSchedule->line;

    uint16_t index;
    uint32_t abs_minutes_sched;
    uint32_t abs_minutes_curr;

    watch_date_time date_time = watch_rtc_get_date_time();
    abs_minutes_curr = date_time.unit.hour * 60 + date_time.unit.minute;

    for (int trainNumber = 0; trainNumber<currLine->trains; trainNumber++) {
	    index =  stationNumber + trainNumber * currLine->stations;
	    *sched = lineSchedule->schedArr[index];

        // special undefined values mean that train doesn't stop in this station
        if (sched->hour == 31 && sched->minute == 63) {
            continue;
        }

	    abs_minutes_sched = sched->hour * 60 + sched->minute;
	    if (abs_minutes_sched > abs_minutes_curr) {
		    return 0;
	    }
    }

    // we find first train of the day
    for (int trainNumber = 0; trainNumber<currLine->trains; trainNumber++) {
        index =  stationNumber + trainNumber * currLine->stations;
        *sched = lineSchedule->schedArr[index];
        if (sched->hour != 31 && sched->minute != 63) {
            break;
        }
    }
    return 1;

}

static void update_screen_station_sel(uint8_t lineNumber, uint8_t stationNumber, bool rel_mode) {

    char buf[7];
    uint16_t hour = 0;
    uint16_t min = 0;
    uint16_t time_diff_minutes = 0;
    SchedCell next_train;

    const Stations *lineStations = stations[lineNumber];
    const Line * curr_line = lineStations->line;
    const int no_train_found = seek_next_train(lineNumber, stationNumber, &next_train);
    const watch_date_time date_time = watch_rtc_get_date_time();

    // first station short name
    char buf_sn[3] = "   ";  // with the last we override the 3rd char
    sprintf(buf_sn, "%s", lineStations->stationNameArr[stationNumber]);
    watch_display_string(buf_sn, 0);

    // station number
    char buf_ln[3] = "   ";
    sprintf(buf_ln, "%2d", stationNumber+1);
    watch_display_string(buf_ln, 2);

    watch_clear_indicator(WATCH_INDICATOR_LAP);

    if (rel_mode) {
	    watch_clear_indicator(WATCH_INDICATOR_24H);
	    uint16_t sched_min = next_train.hour * 60 + next_train.minute;
	    uint16_t date_min = date_time.unit.hour * 60 + date_time.unit.minute;
	    if (no_train_found == 1)
		    time_diff_minutes += 1440 ;
	    time_diff_minutes += sched_min - date_min;
	    hour = time_diff_minutes / 60;
	    min = time_diff_minutes % 60;
    }
    else {
	    if (no_train_found == 1)
		    watch_set_indicator(WATCH_INDICATOR_LAP);

	    watch_set_indicator(WATCH_INDICATOR_24H);
	    hour = next_train.hour;
	    min = next_train.minute;
    }

    watch_set_colon();

    // next train diff or hour
    sprintf(buf, "%02d%02d%02d", hour, min, curr_line->stations);
    watch_display_string(buf, 4);
}

static bool line_sel_event_handler(movement_event_t event, movement_settings_t *settings, void *context) {
    ferrochrono_state_t *state = (ferrochrono_state_t *)context;

    switch (event.event_type) {
        case EVENT_ACTIVATE:
            update_screen_line_sel(state->rw_line);
            break;
        case EVENT_LIGHT_BUTTON_UP:
            // previous railway line
            if (state->rw_line == 0) {
                state->rw_line = NUM_LINES - 1;
            } else {
                state->rw_line = (state->rw_line - 1) % NUM_LINES;
            }
            update_screen_line_sel(state->rw_line);
            break;
        case EVENT_LIGHT_BUTTON_DOWN:
            // leave empty for no light
            break;
        case EVENT_ALARM_BUTTON_UP:
            // next railway line
            state->rw_line = (state->rw_line + 1) % NUM_LINES;
            update_screen_line_sel(state->rw_line);
            break;
        case EVENT_ALARM_LONG_PRESS:
            state->mode = rw_sched;
            // select current line
            update_screen_station_sel(state->rw_line, state->rw_station, state->show_rel);
            break;
        default:
            return movement_default_loop_handler(event, settings);
    }
    return true;
}

static bool schedule_event_handler(movement_event_t event, movement_settings_t *settings, void *context) {
    ferrochrono_state_t *state = (ferrochrono_state_t *)context;

    const Line * currLine = stations[state->rw_line]->line;

    watch_date_time date_time;

    switch (event.event_type) {
        case EVENT_ACTIVATE:
            update_screen_station_sel(state->rw_line, state->rw_station, state->show_rel);
            break;
        case EVENT_LIGHT_BUTTON_UP:
            // previous station
            if (state->rw_station == 0) {
                state->rw_station = currLine->stations - 1;
            } else {
                state->rw_station = (state->rw_station - 1) % currLine->stations;
            }
            update_screen_station_sel(state->rw_line, state->rw_station, state->show_rel);
            break;
        case EVENT_LIGHT_LONG_PRESS:
            // return to line select
            state->mode = rw_select;
            state->rw_station = 0;
            update_screen_line_sel(state->rw_line);
            break;
        case EVENT_LIGHT_BUTTON_DOWN:
            // leave empty for no light
            break;
        case EVENT_ALARM_BUTTON_UP:
            // next station
            state->rw_station = (state->rw_station + 1) % currLine->stations;
            update_screen_station_sel(state->rw_line, state->rw_station, state->show_rel);
            break;
        case EVENT_ALARM_LONG_PRESS:
            // show better display
            state->show_rel = ! state->show_rel;
            update_screen_station_sel(state->rw_line, state->rw_station, state->show_rel);
            break;
        case EVENT_TICK:
            // countdown
            date_time = watch_rtc_get_date_time();
            if (date_time.unit.minute == 0) {
                update_screen_station_sel(state->rw_line, state->rw_station, state->show_rel);
            }
            break;
        default:
            return movement_default_loop_handler(event, settings);
    }
    return true;
}


bool ferrochrono_face_loop(movement_event_t event, movement_settings_t *settings, void *context) {
    ferrochrono_state_t *state = (ferrochrono_state_t *)context;

    switch (state->mode) {
        case rw_select:
            return line_sel_event_handler(event, settings, context);
            break;
        case rw_sched:
            return schedule_event_handler(event, settings, context);
            break;
    }

    switch (event.event_type) {
        case EVENT_ACTIVATE:
            // This is almost always handled by the above handlers
            break;
        case EVENT_TIMEOUT:
            // Your watch face will receive this event after a period of inactivity. If it makes sense to resign,
            // you may uncomment this line to move back to the first watch face in the list:
            // movement_move_to_face(0);
            break;
        case EVENT_LOW_ENERGY_UPDATE:
            // If you did not resign in EVENT_TIMEOUT, you can use this event to update the display once a minute.
            // Avoid displaying fast-updating values like seconds, since the display won't update again for 60 seconds.
            // You should also consider starting the tick animation, to show the wearer that this is sleep mode:
            // watch_start_tick_animation(500);
            break;
        default:
            // Movement's default loop handler will step in for any cases you don't handle above:
            // * EVENT_LIGHT_BUTTON_DOWN lights the LED
            // * EVENT_MODE_BUTTON_UP moves to the next watch face in the list
            // * EVENT_MODE_LONG_PRESS returns to the first watch face (or skips to the secondary watch face, if configured)
            // You can override any of these behaviors by adding a case for these events to this switch statement.
            return movement_default_loop_handler(event, settings);
    }

    // return true if the watch can enter standby mode. Generally speaking, you should always return true.
    // Exceptions:
    //  * If you are displaying a color using the low-level watch_set_led_color function, you should return false.
    //  * If you are sounding the buzzer using the low-level watch_set_buzzer_on function, you should return false.
    // Note that if you are driving the LED or buzzer using Movement functions like movement_illuminate_led or
    // movement_play_alarm, you can still return true. This guidance only applies to the low-level watch_ functions.
    return true;
}

void ferrochrono_face_activate(movement_settings_t *settings, void *context) {
    (void) settings;
    ferrochrono_state_t *state = (ferrochrono_state_t *)context;
    // Handle any tasks related to your watch face coming on screen.
}


void ferrochrono_face_resign(movement_settings_t *settings, void *context) {
    (void) settings;
    (void) context;
    // handle any cleanup before your watch face goes off-screen.
}
